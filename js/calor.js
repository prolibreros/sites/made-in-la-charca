// Cuando se carga el documento
window.onload = function () {
  // Precarga elementos
  queue.loadManifest([
    {id: 'video', src: '../../video/calor.mp4'},
  ]);

  // Navegación
  queue.on('complete', function () {
    init_nav([
      {
        parent: document.getElementsByTagName('div')[0],
        x: .5,
        y: .2,
        width: 100,
        height: 100,
        label: 'álbum',
        href: '../album/index.html',
        vidTime: 37.5,
        life: 3.9
      }
    ]);
  }, this);
}
