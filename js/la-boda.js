// Cuando se han cargado los elementos
function preload_complete (e) {
  play_audio('sound', true);
  insert_video_full();
}

// Cuando se carga el documento
window.onload = function () {
  // Precarga elementos
  queue.loadManifest([
    {id: 'sound', src: '../../sound/intro.mp3'},
    {id: 'video', src: '../../video/la-boda.mp4'}
  ]);

  subtitles('la-boda-subs', [
    {start: 0, end: 3, text: 'Tenemos la esperanza en ti.'},
    {start: 3, end: 6, text: '¿No ves qué eres la única que queda?'},
    {start: 6, end: 9, text: 'Jajaja.'},
    {start: 20, end: 23, text: '¿Es la casa de ellos?'},
    {start: 23, end: 26, text: 'Sí.'},
    {start: 26, end: 29, text: 'Ese es su hermano de ella.'},
    {start: 29, end: 32, text: '¿Y esa camioneta?'},
    {start: 32, end: 35, text: 'Antes era de Matías, ahora es de Carlen.'},
    {start: 52, end: 55, text: 'Esa señora es hermana de mi tía Felix.'},
    {start: 55, end: 58, text: 'Ella es mi tía Rosa, hermana de mi papá…'},
    {start: 58, end: 61, text: '…también vive en EU.'},
    {start: 61, end: 64, text: '¿Ella en qué trabaja?'},
    {start: 64, end: 67, text: 'Hacía comida y cosas así.'},
    {start: 67, end: 70, text: 'Ahorita Carlen se fue a ver a Antony Santos.'},
    {start: 70, end: 73, text: '¿Quién es Antony Santos?'},
    {start: 73, end: 76, text: 'Un bachatero, ¿no lo has oído cantar?'},
    {start: 76, end: 79, text: 'No.'},
    {start: 79, end: 82, text: 'A esa ni la panza la detiene…'},
    {start: 82, end: 85, text: '¿Por qué la panza?'},
    {start: 85, end: 88, text: 'Está embarazada'},
    {start: 88, end: 91, text: '¿cuántos meses tiene?'},
    {start: 91, end: 94, text: '5 meses…'},
    {start: 94, end: 97, text: 'Vive en un barrio latino'},
    {start: 97, end: 100, text: 'En esa iglesia dan misas:'},
    {start: 100, end: 103, text: 'una hora en español, otra en portugués…'},
    {start: 142, end: 145, text: '¿Aquí aún hay bodas y XV años?'},
    {start: 145, end: 148, text: 'Antes había un montón ¿Verdad tía?'},
    {start: 148, end: 151, text: 'Eran unas comidonas antes, barbacoa, tamales…'},
    {start: 151, end: 154, text: 'Ya casi no hay, es que está dura la cosa…'},
    {start: 165, end: 170, text: 'Es que los jóvenes se van.'},
  ], true);

  // Navegación
  queue.on('complete', function () {
    init_nav([
      {
        parent: document.getElementsByTagName('video')[0],
        x: .3,
        y: .6,
        width: 100,
        height: 100,
        label: 'fotos y recuerdos',
        href: '../fotos-recuerdos/index.html',
        vidTime: 83,
        life: 4
      },
      {
        parent: document.getElementsByTagName('video')[0],
        x: .5,
        y: .5,
        width: 100,
        height: 100,
        label: 'fotos y recuerdos',
        href: '../fotos-recuerdos/index.html',
        vidTime: 165,
        life: 3
      }
    ]);
  }, this);
}
